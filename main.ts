// import { exec } from "child_process";
const { exec } = require("child_process");
const colors = require("colors");

/* Handle args */
const device = "Logitech MX Master 3";
const libInputAccelSpeed = -0.63;
let id;

exec("xinput", (error, stdout, stderr) => {
  /* Error Handling*/
  if (error) {
    return console.error(`exec error: ${error}`.red);
  } else if (stderr) {
    return console.error(colors.red({ stderr }));
  }

  /* Get the id */
  stdout.split("\n").forEach(row => {
    if (row.includes(device) && row.includes("pointer")) {
      const indexOfId = row.indexOf("id=");
      const intId = parseInt(row.slice(indexOfId + 3, indexOfId + 6));
      return id = intId;
    }
  });

  /* Run the correct command */
  exec(`xinput set-prop ${id} "libinput Accel Speed" ${libInputAccelSpeed}`, (error, stdout, stderr) => {
    /* Error Handling*/
    if (error) {
      return console.error(`exec error: ${error}`.red);
    } else if (stderr) {
      return console.error(colors.red({ stderr }));
    }

    /* Success */
    if (stdout) {
      console.log(stdout.green);
    }
    console.log(`Changed ${device} [${id}] "libinput Accel Speed" to ${libInputAccelSpeed}`.green);
  });
});
