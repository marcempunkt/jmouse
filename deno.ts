const device: string = "Logitech MX Master 3";
const libInputAccelSpeed: number = -0.63;

const xinput = Deno.run({ cmd: ["xinput"], stderr: "piped", stdout: "piped", stdin: "null" });

const output: string = new TextDecoder().decode(await xinput.output());
const status = await xinput.status();
const stderr = new TextDecoder().decode(await xinput.stderrOutput());

xinput.close();

if (!stderr && status.success) {
  /* Only run when the xinput command was successfully run */
  output.split("\n").every(async (row: string) => {
    if (row.includes(device) && row.includes("pointer")) {
      const indexOfId = row.indexOf("id=");
      const id = parseInt(row.slice(indexOfId + 3, indexOfId + 6));

      const xinputSetProp = Deno.run({
	cmd: ["xinput", "set-prop", String(id), "libinput Accel Speed", String(libInputAccelSpeed)],
	stderr: "piped",
	stdout: "piped",
	stdin: "null"
      });

      const setPropStatus = await xinputSetProp.status();

      if (setPropStatus.success) {
	console.log(`Changed ${device} [${id}] "libinput Accel Speed" to ${libInputAccelSpeed}`);
      } else {
	console.error(`Couldn't set ${device} [${id}] "libinput Accel Speed to ${libInputAccelSpeed}"`);
      }
      /* Stop the loop */
      return false;
    } else {
      return true;
    }
  });
}


